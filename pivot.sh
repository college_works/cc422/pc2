#!/bin/bash
#
# This script create an EC2 instance used to access the deployed DB Cluster

keyPair='aws-virg'
instanceType=t2.micro
instanceName='pivot'

ami=$(aws ssm get-parameter \
        --name /aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2 \
        --query "Parameter.Value" \
        --output text)

echo "AMI: $ami"

# VPC and Subnet
vpcId=$(aws ec2 describe-vpcs \
        --query 'Vpcs[?IsDefault==`true`].VpcId' \
        --output text)

echo "vpc: $vpcId"

subnetId=$(aws ec2 describe-subnets \
            --filter "Name=vpc-id,Values=$vpcId" \
            --query "Subnets[0].SubnetId" \
            --output text)

echo "Subnet: $subnetId"


# SSH security group
ssh_sg=$(aws ec2 describe-security-groups \
          --group-names allowSSH \
          --query "SecurityGroups[*].GroupId" \
          --output text 2> /dev/null)

if [[ -z $ssh_sg ]]; then
    echo "[*] Creating allowSSH security group"
    ssh_sg=$(aws ec2 create-security-group \
                --group-name allowSSH \
                --description "Allow SSH Access" \
                --query "GroupId" \
                --vpc-id $vpcId \
                --output text)
fi

echo "SSH security group: $ssh_sg"

# Adding ingress to SSH security group
aws ec2 authorize-security-group-ingress \
  --group-id $ssh_sg \
  --protocol tcp \
  --port 22 \
  --cidr 0.0.0.0/0 2> /dev/null


# Launching pivot instance
instanceId=$(aws ec2 describe-instances \
              --filters "Name=tag:Name,Values=$instanceName" \
                        "Name=instance-state-name,Values=running" \
              --query 'Reservations[*].Instances[*].InstanceId' \
              --output text)

if [[ -z $instanceId ]]; then
  echo "Creating instance $instanceName"

  instanceId=$(aws ec2 run-instances \
                --image-id $ami \
                --instance-type $instanceType \
                --key-name $keyPair \
                --security-group-ids $ssh_sg \
                --subnet-id $subnetId \
                --user-data file://$PWD/mysql.txt \
                --associate-public-ip-address \
                --tag-specifications "ResourceType=instance,Tags=[{Key=Name,Value=$instanceName}]" \
                --query 'Instances[*].InstanceId' \
                --output text)
fi

echo "Waiting for instance $instanceId ..."
aws ec2 wait instance-running --instance-ids $instanceId

aws ec2 describe-instances --instance-ids $instanceId \
    --filters 'Name=instance-state-name,Values=running' \
    --query 'Reservations[*].Instances[*]' \
    | grep -E "KeyName|PublicIpAddress|Platform|PublicDnsName" | uniq