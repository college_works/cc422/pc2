# CC422 - PC2

NOTE: This repository was moved to https://gitlab.com/college_works/cc422-20231/homeworks/homework2

**THEME:** Multi-AZ DB Cluster - AWS RDS (engine: Aurora-MySQL)

In this homework, we have deploy the following infrastructure:
![Multi-AZ DB cluster](./images/infrastructure.png)


# QuickStart
```sh
# run bash script to deploy infrastructure
$ bash rds.sh
```